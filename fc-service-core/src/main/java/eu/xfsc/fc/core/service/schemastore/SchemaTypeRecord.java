package eu.xfsc.fc.core.service.schemastore;

import eu.xfsc.fc.core.service.schemastore.SchemaStore.SchemaType;

public record SchemaTypeRecord(SchemaType type, String schemaId) {
}
